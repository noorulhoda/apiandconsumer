﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using B2BService;
using Microsoft.AspNetCore.Mvc;
using soapConsumer;
using WebApiIdentityTokenAuth.Models;



namespace B2BAPI.Controllers
{
    [System.Web.Http.Route("api/B2B")]
    public class B2BController : Controller
    {
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetRateQuote")]
        public async Task<JsonResult> GetRate()//ShipmentDetails s)
        {
            Consumer consumer = new Consumer();
            ShipmentDetails s = new ShipmentDetails("FL", "USA", "FL", "USA", "50.0", 250.5f);
            string response = consumer.GetRateQuote(s);

            return Json(new Response(HttpStatusCode.OK)
            {
                Message = "done"
            }) ;
        }

        
        [System.Web.Http.Route("GetPalletTypes")]
        public async Task<JsonResult> GetPalletTypes()//ShipmentDetails s)
        {
            Consumer consumer = new Consumer();
            ShipmentDetails s = new ShipmentDetails("FL", "USA", "FL", "USA", "50.0", 250.5f);
            PalletType[] response = consumer.GetPalletTypes();

            return Json(new Response(HttpStatusCode.OK)
            {
                Message = response.ToString()
            }) ;
        }


    }
}

