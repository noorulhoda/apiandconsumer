﻿using B2BAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using soapConsumer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebApiIdentityTokenAuth.Models;

namespace B2BAPI.Controllers
{



    [Route("api/user")]
    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
  
    public AccountController(
        UserManager<IdentityUser> userManager,
        SignInManager<IdentityUser> signInManager)
    
    {
        _userManager = userManager;
        _signInManager = signInManager;
     
    }







    [HttpPost]
    [Route("register")]
    public async Task<JsonResult> Register(User user)
    {
            string email = user.email;
            string password = user.password;
            string confirmPassword = user.password;

        if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(password))
        {
                return Json(new Response(HttpStatusCode.BadRequest)
                {
                    Message = "email or password is nulllllllll" + email +"pass:"+ password
                });
        }

        if (password != confirmPassword)
        {
            return Json(new Response(HttpStatusCode.BadRequest)
            {
                Message = "Passwords don't match!"
            });
        }

        var newUser = new IdentityUser
        {
            UserName = email,
            Email = email
        };

        IdentityResult userCreationResult = null;
        try
        {
            userCreationResult = await _userManager.CreateAsync(newUser, password);
        }
        catch (SqlException)
        {
            return Json(new Response(HttpStatusCode.InternalServerError)
            {
                Message = "Error communicating with the database, see logs for more details"
            });
        }

        if(!userCreationResult.Succeeded)
        {
            return Json(new Response(HttpStatusCode.BadRequest)
            {
                Message = "An error occurred when creating the user, see nested errors",
                Errors = userCreationResult.Errors.Select(x => new Response(HttpStatusCode.BadRequest)
                {
                    Message = $"[{x.Code}] {x.Description}"
                })
            });
        }

        return Json(new Response(HttpStatusCode.OK)
        {
            Message = $"Registration completed"
        });
    }











    [HttpPost]
    [Route("login")]
    public async Task<JsonResult> Login(string email, string password)
    {
        if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(password))
        {
            return Json(new Response(HttpStatusCode.BadRequest)
            {
                Message = "email or password is null"
            });
        }

        var user = await _userManager.FindByEmailAsync(email);
        if (user == null)
        {
            return Json(new Response(HttpStatusCode.BadRequest)
            {
                Message = "Invalid Login and/or password"
            });
        }

        if (!user.EmailConfirmed)
        {
            return Json(new Response(HttpStatusCode.BadRequest)
            {
                Message = "Email not confirmed, please check your email for confirmation link"
            });
        }

        var passwordSignInResult = await _signInManager.PasswordSignInAsync(user, password, isPersistent: true, lockoutOnFailure: false);
        if (!passwordSignInResult.Succeeded)
        {
            return Json(new Response(HttpStatusCode.BadRequest)
            {
                Message = "Invalid Login and/or password"
            });
        }

        return Json(new Response(HttpStatusCode.OK)
        {
            Message = "Cookie created"
        });
    }











    [HttpPost]
    [Route("logout")]
    public async Task<JsonResult> Logout()
    {
        await _signInManager.SignOutAsync();

        return Json(new Response(HttpStatusCode.OK)
        {
            Message = "You have been successfully logged out"
        });
    }












}



}
