﻿using System;
using B2BService;
using static B2BService.RateQuoteServiceSoapClient;
using System.Xml;
namespace soapConsumer
{
    public class Consumer
    {
        EndpointConfiguration configuration;
        RateQuoteServiceSoapClient client;
        string APIKey = "YtM2MThzE3M1JiNjMtMWNhNi00MDA5LWEzMjZTQ0MkN2NzQjC";
        RateQuoteRequest request;

        public Consumer()
        {
            configuration = new EndpointConfiguration();
            client = new RateQuoteServiceSoapClient(configuration);
           
        }

        public  PalletType[] GetPalletTypes()
        {
            var response = client.GetPalletTypesAsync(APIKey);
            PalletType[]  responseList = response.Result.Result;
            foreach (var i in responseList)
            {
                Console.WriteLine(i.ToString());
            }
            return responseList;
        }

        public string GetRateQuote(ShipmentDetails shipmentDetails)
        {
            request = new RateQuoteRequest();
            request.Origin = new ServicePoint();
            request.Origin.ZipOrPostalCode = shipmentDetails.originZipCode;
            request.Origin.CountryCode = shipmentDetails.originCountryCode;
            request.Destination = new ServicePoint();
            request.Destination.CountryCode = shipmentDetails.destCountryCode;
            request.Destination.ZipOrPostalCode = shipmentDetails.destZipCode;
            Item item = new Item(); 
            item.Class =shipmentDetails.itemClass; 
            item.Weight = shipmentDetails.weight;
            Item[] items = { item };
            request.Items = items;
            var response = client.GetRateQuoteAsync(APIKey, request);
            string customerData = response.Result.CustomerData;
            bool wasSuccess = response.Result.WasSuccess;
            Console.WriteLine($"customerData: {customerData}| wasSuccess:{wasSuccess}");
            return $"customerData: {customerData}| wasSuccess:{wasSuccess}";



        }
        

 


    }
}
