﻿using System;
using System.Collections.Generic;
using System.Text;

namespace soapConsumer
{
    public class ShipmentDetails
    {
        public string originZipCode;
        public string originCountryCode;
        public string destZipCode;
        public string destCountryCode;
        public string itemClass;
        public float weight;
        public ShipmentDetails(string oZCode,string oCCode, string dZCode, string dCCode,string itmClass,float w)
        {
            originZipCode = oZCode;
            originCountryCode = oCCode;
            destZipCode = dZCode;
            destCountryCode = dCCode;
            itemClass = itmClass;
            weight=w;
        }
     
        
    }

    /*public enum itemClass
    {
        "50.0" ,
        "55.0" ,
        "60.0" ,
        "65.0",
        "70.0" ,
        "77.5" , 
        "85.0" ,
        "92.5" , 
        "100.0",
        "500.0"
    }*/
}
